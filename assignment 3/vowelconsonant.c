#include <stdio.h>
//This program will check whether the given character is a Vowel or a Consonant.
int main()
{
	char letter;
	printf("Enter the character: \n");
	scanf("%c",&letter);
	if (letter=='a'||letter=='e'||letter=='i'||letter=='o'||letter=='u')
	printf("%c is a Vowel\n",letter);
	else if (letter=='A'||letter=='E'||letter=='I'||letter=='O'||letter=='U')
	printf("%c is a Vowel\n",letter);
	else
	printf("%c is a Consonant\n",letter);
	return 0;

}
