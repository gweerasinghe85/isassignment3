#include <stdio.h>
//This program will check whether the given value is positive, negative or zero.
int main()
{
	int a;
	printf("Enter the number to check: \n");
	scanf("%d",&a);
	if (a>0)
	printf("%d is a positive number.\n",a);
	else if (a<0)
	printf("%d is a negative number.\n",a);
	else 
	printf("This is 0.\n");
	return 0;
}
